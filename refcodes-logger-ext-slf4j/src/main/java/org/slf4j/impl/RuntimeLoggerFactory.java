// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.slf4j.impl;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerConsts;
import org.refcodes.logger.impls.RuntimeLoggerFactorySingleton;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.bridge.SLF4JBridgeHandler;
import org.slf4j.helpers.NOPLogger;

public class RuntimeLoggerFactory implements ILoggerFactory {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static final String BASTARD_RECURSIVE_DEPENDENCY_FROM_HELL = "org.apache.commons";
	private ConcurrentMap<String, Logger> _nameToLoggerMap;
	private boolean _hasBootStrapped = false;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	public RuntimeLoggerFactory() {
		_nameToLoggerMap = new ConcurrentHashMap<String, Logger>();
		SLF4JBridgeHandler.install();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	@Override
	public Logger getLogger( String name ) {
		if ( !_hasBootStrapped && name.startsWith( BASTARD_RECURSIVE_DEPENDENCY_FROM_HELL ) ) return NOPLogger.NOP_LOGGER;
		Logger slf4jLogger = _nameToLoggerMap.get( name );
		if ( slf4jLogger != null ) {
			return slf4jLogger;
		}
		else {
			if ( name != null ) {
				name = name.replaceAll( "/", "." );
			}
			if ( name.length() > 0 && name.charAt( 0 ) == '.' ) {
				name = name.substring( 1 );
			}
			if ( name == null || name.length() == 0 ) {
				name = RuntimeLoggerConsts.ROOT_LOGGER_ELEMENT_PATH;
			}
			RuntimeLogger theRuntimeLogger;
			if ( name.equalsIgnoreCase( Logger.ROOT_LOGGER_NAME ) ) theRuntimeLogger = RuntimeLoggerFactorySingleton.createRuntimeLogger();
			else theRuntimeLogger = RuntimeLoggerFactorySingleton.getInstance().createInstance( name );
			Logger newInstance = new RuntimeLoggerAdapter( theRuntimeLogger );
			Logger oldInstance = _nameToLoggerMap.putIfAbsent( name, newInstance );
			_hasBootStrapped = true;
			return oldInstance == null ? newInstance : oldInstance;
		}
	}
}
