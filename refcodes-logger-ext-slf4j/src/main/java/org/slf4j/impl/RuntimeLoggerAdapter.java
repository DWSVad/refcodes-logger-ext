// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.slf4j.impl;

import java.io.Serializable;

import org.refcodes.logger.LogPriority;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerAccessor;
import org.refcodes.logger.alt.slf4j.impls.Slf4jRuntimeLoggerFactorySingleton;
import org.refcodes.logger.alt.slf4j.impls.Slf4jRuntimeLoggerImpl;
import org.slf4j.Marker;
import org.slf4j.helpers.MarkerIgnoringBase;
import org.slf4j.spi.LocationAwareLogger;

public final class RuntimeLoggerAdapter extends MarkerIgnoringBase implements RuntimeLoggerAccessor, LocationAwareLogger, Serializable {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	// final static String FQCN = RuntimeLoggerAdapter.class.getName();

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	final transient RuntimeLogger logger;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	public RuntimeLoggerAdapter( RuntimeLogger logger ) {
		this.logger = logger;
		this.name = logger.getName();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	@Override
	public boolean isTraceEnabled() {
		return true;
	}

	@Override
	public void trace( String msg ) {
		logger.trace( msg );
	}

	@Override
	public void trace( String format, Object arg ) {
		logger.trace( format, arg );
	}

	@Override
	public void trace( String format, Object arg1, Object arg2 ) {
		logger.trace( format, arg1, arg2 );
	}

	@Override
	public void trace( String format, Object... args ) {
		logger.trace( format, args );
	}

	@Override
	public void trace( String msg, Throwable t ) {
		logger.trace( msg, t );
	}

	@Override
	public boolean isDebugEnabled() {
		return logger.isLogDebug();
	}

	@Override
	public void debug( String msg ) {
		logger.debug( msg );
	}

	@Override
	public void debug( String format, Object arg ) {
		logger.debug( format, arg );
	}

	@Override
	public void debug( String format, Object arg1, Object arg2 ) {
		logger.debug( format, arg1, arg2 );
	}

	@Override
	public void debug( String format, Object... args ) {
		logger.debug( format, args );
	}

	@Override
	public void debug( String msg, Throwable t ) {
		logger.debug( msg, t );
	}

	@Override
	public boolean isInfoEnabled() {
		return logger.isLogInfo();
	}

	@Override
	public void info( String msg ) {
		logger.info( msg );
	}

	@Override
	public void info( String format, Object arg ) {
		logger.info( format, arg );
	}

	@Override
	public void info( String format, Object arg1, Object arg2 ) {
		logger.info( format, arg1, arg2 );
	}

	@Override
	public void info( String format, Object... args ) {
		logger.info( format, args );
	}

	@Override
	public void info( String msg, Throwable t ) {
		logger.info( msg, t );
	}

	@Override
	public boolean isWarnEnabled() {
		return logger.isLogWarn();
	}

	@Override
	public void warn( String msg ) {
		logger.warn( msg );
	}

	@Override
	public void warn( String format, Object arg ) {
		logger.warn( format, arg );
	}

	@Override
	public void warn( String format, Object arg1, Object arg2 ) {
		logger.warn( format, arg1, arg2 );
	}

	@Override
	public void warn( String format, Object... args ) {
		logger.warn( format, args );
	}

	@Override
	public void warn( String msg, Throwable t ) {
		logger.warn( msg, t );
	}

	@Override
	public boolean isErrorEnabled() {
		return logger.isLogError();
	}

	@Override
	public void error( String msg ) {
		logger.error( msg );
	}

	@Override
	public void error( String format, Object arg ) {
		logger.error( format, arg );
	}

	@Override
	public void error( String format, Object arg1, Object arg2 ) {
		logger.error( format, arg1, arg2 );
	}

	@Override
	public void error( String format, Object... args ) {
		logger.error( format, args );
	}

	@Override
	public void error( String msg, Throwable t ) {
		logger.error( msg, t );
	}

	@Override
	public void log( Marker marker, String callerFQCN, int level, String msg, Object[] args, Throwable t ) {
		LogPriority logPriority;
		switch ( level ) {
		case LocationAwareLogger.TRACE_INT:
			logPriority = LogPriority.TRACE;
			break;
		case LocationAwareLogger.DEBUG_INT:
			logPriority = LogPriority.DEBUG;
			break;
		case LocationAwareLogger.INFO_INT:
			logPriority = LogPriority.INFO;
			break;
		case LocationAwareLogger.WARN_INT:
			logPriority = LogPriority.WARN;
			break;
		case LocationAwareLogger.ERROR_INT:
			logPriority = LogPriority.ERROR;
			break;
		default:
			throw new IllegalStateException( "Level number " + level + " is not recognized." );
		}
		logger.log( logPriority, msg, t );
	}

	/**
	 * In case the {@link Slf4jRuntimeLoggerImpl} (as created by the
	 * {@link Slf4jRuntimeLoggerFactorySingleton}) detects that SLF4J has bound
	 * a {@link RuntimeLoggerAdapter} (i.e. the REFCODES.ORG SLF4J binding), it
	 * directly delegates its method calls to the wrapped {@link RuntimeLogger}
	 * instead of marshaling a log request through this
	 * {@link RuntimeLoggerAdapter}; as marshaling would mean consolidating of
	 * various detailed {@link LogPriority} levels to a single SLF4J log level.
	 * 
	 * @return The wrapped {@link RuntimeLogger} used by the
	 *         {@link Slf4jRuntimeLoggerImpl} when possible.
	 */
	@Override
	public RuntimeLogger getRuntimeLogger() {
		return logger;
	}
}
