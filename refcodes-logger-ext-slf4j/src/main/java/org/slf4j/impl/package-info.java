// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

/**
 * SLF4J is the "simple logging facade for Java". This package contains the
 * SLF4J adapter for the REFCODES.ORG logging framework represened by the
 * {@link org.refcodes.logger.RuntimeLogger} and the more generic
 * {@link org.refcodes.logger.Logger} types.
 * <p>
 * This implementation is based on a provided logger adapter (as shipped with
 * the SLF4J sources [1]) and tailored according to the SLF4J FAQ [2].
 * <p>
 * "... It is important to create these files in the package: org.slf4j.impl,
 * because SLF4J is using this to find the correct adapter ..." [3] This means
 * that the package structure for the adapter is given and must not be changed.
 * <p>
 * Regarding the usage of SLF4J, please refere to the official SLF4J
 * documentation [4].
 *
 * @see "[1] http://www.slf4j.org/download.html"
 * @see "[2] http://www.slf4j.org/faq.html#slf4j_compatible"
 * @see "[3] http://javaeenotes.blogspot.de/2011/12/custom-slf4j-logger-adapter.html"
 * @see "[4] http://www.slf4j.org"
 */
package org.slf4j.impl;