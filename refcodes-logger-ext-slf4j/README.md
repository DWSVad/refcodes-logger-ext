# README #

"*The [`REFCODES.ORG`](http://www.refcodes.org) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.*"

### What is this repository for? ###

You can use the [`refcodes-logger`](https://bitbucket.org/refcodes/refcodes-logger) framework itself as an [`SLF4J`](http://www.slf4j.org) binding in order to make, for example, your [`Tomcat`](http://tomcat.apache.org/) or [`spring-boot`](http://projects.spring.io/spring-boot) log to the fancy [`refcodes-logger-alt-console`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/master/refcodes-logger-alt-console) or a [`NoSQL`](http://en.wikipedia.org/wiki/NoSQL) DB cluster (as of [`refcodes-logger-alt-simpledb`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/master/refcodes-logger-alt-simpledb)) ...

### How do I get set up? ###

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:


```
<dependencies>
	...
	<dependency>
		<artifactId>refcodes-logger-ext-slf4j</artifactId>
		<groupId>org.refcodes</groupId>
		<version>1.0.0</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-logger-ext/src/master/refcodes-logger-ext-slf4j). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-logger-ext-slf4j).

### How do I get started? ###

In addition you have to add a dependency of one of the [`refcodes-logger-alt-*`](https://bitbucket.org/refcodes/refcodes-logger-alt) artifacts to your `pom.xml` and an according [`runtimelogger-config.xml`](https://bitbucket.org/refcodes/refcodes-logger/src/master/src/test/resources/runtimelogger-config.xml) pulling that logger implementation you want your [`SLF4J`](http://www.slf4j.org) logs to log to ...

> To bind [`SLF4J`](http://www.slf4j.org) to, for example, the "Console-based" [`refcodes-logger-alt-console`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/master/refcodes-logger-alt-console) logger, add the below dependency (in addition to the above one):

```
<dependencies>
	...
	<dependency>
		<artifactId>refcodes-logger-alt-console</artifactId>
		<groupId>org.refcodes</groupId>
		<version>1.0.0</version>
	</dependency>
	...
</dependencies>
```

> Attention: Never use the [`SLF4J`](http://www.slf4j.org) binding [`refcodes-logger-ext-slf4j`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/master/refcodes-logger-ext-slf4j) together with the [`refcodes-logger-alt-slf4j`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/master/refcodes-logger-alt-slf4j) logger implementation, else the one will look for the other, the other for the one ... until you get an `StackOverflowException` :D
 
With the [`SLF4J`](http://www.slf4j.org) binding, you can log all your [`SLF4J`](http://www.slf4j.org) logs (also the ones of the libs you included in your app) to any of the [`refcodes-logger-alt-*`](https://bitbucket.org/refcodes/refcodes-logger-alt) implementations:

> Make your `microservices` log into a [`NoSQL`](http://en.wikipedia.org/wiki/NoSQL) database cluster with the  [`refcodes-logger-alt-simpledb`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/master/refcodes-logger-alt-simpledb) artifact providing support for Amazon's [`SimpleDB`](http://aws.amazon.com/de/simpledb).

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
* Make  [`refcodes-logger-ext-slf4j`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/master/refcodes-logger-ext-slf4j) detect an [`refcodes-logger-alt-slf4j`](https://bitbucket.org/refcodes/refcodes-logger-alt/src/master/refcodes-logger-alt-slf4j) infinite initialization loop

### Who do I talk to? ###

* Siegfried Steiner (steiner@refcodes.org)

### Terms and conditions ###

The [`REFCODES.ORG`](http://www.refcodes.org) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.