# README #

"*The [`REFCODES.ORG`](http://www.refcodes.org) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.*"

### What is this repository for? ###

The [`refcodes-logger-ext`](https://bitbucket.org/refcodes/refcodes-logger-ext) artifact extends the types and implementations as defined by the [`refcodes-logger`](https://bitbucket.org/refcodes/refcodes-logger) artifact with additional (extended) functionality. See the [`refcodes-logger-ext-slf4j`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/master/refcodes-logger-ext-slf4j) artifact which binds the [`SLF4J`](http://www.slf4j.org) logger facade to the [`refcodes-logger`](https://bitbucket.org/refcodes/refcodes-logger) framework; taking over all [`SLF4J`](http://www.slf4j.org) logs as being your [`SLF4J`](http://www.slf4j.org) binding.

### How do I get set up? ###

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<groupId>org.refcodes</groupId>
		<artifactId>refcodes-logger-ext</artifactId>
		<version>1.0.0</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-logger-ext). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-logger-ext).

### How do I get started? ###

Directly jump into the (sub-)modules:

* [`refcodes-logger-ext-slf4j`](https://bitbucket.org/refcodes/refcodes-logger-ext/src/master/refcodes-logger-ext-slf4j)

### Contribution guidelines ###

* See the according (sub-)modules of this artifact.

### Who do I talk to? ###

* Siegfried Steiner (steiner@refcodes.org)

### Terms and conditions ###

The [`REFCODES.ORG`](http://www.refcodes.org) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.